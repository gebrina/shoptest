<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

 // relationships
 public function brand() {
   return $this->belongsTo(Brand::class);
 }

 public function colors() {
   return $this->belongsToMany(Color::class);
 }

 public function make_unavailable() {
   $this->available = false;
   $this->save();
 }

  protected $fillable = ['name', 'body', 'brand_id', 'image'];
}
