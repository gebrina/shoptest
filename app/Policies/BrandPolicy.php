<?php

namespace App\Policies;

use App\Brand;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BrandPolicy
{
    use HandlesAuthorization;

    protected $name = 'brand';

    public function index(User $user)
    {
      if ($user->can("{$this->name}.index")) {
          return true;
      }
    }

    public function edit(User $user)
    {
      if ($user->can("{$this->name}.edit")) {
          return true;
      }
    }

    public function create(User $user)
    {
      if ($user->can("{$this->name}.create")) {
          return true;
      }
    }
}
