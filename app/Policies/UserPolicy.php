<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
      return $user->isAdministrator()
                ? Response::allow()
                : Response::deny('You do not have permissions.');
    }

    public function admin(User $user)
    {
      return $user->administrator;
    }

    public function update(User $user, User $model)
    {
        return $this->isOneself($user, $model);
    }

    protected function isOneself(User $user, $model) {
      return $user->id === $model->id;
    }
}
