<?php

namespace App\Policies;

use App\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        //
    }

    public function view(User $user, Product $product)
    {
        //
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Product $product)
    {
        return $this->isOwner($user, $product);
    }

    public function delete(User $user, Product $product)
    {
        return $this->isOwner($user, $product);
    }

    public function restore(User $user, Product $product)
    {
        //
    }

    public function forceDelete(User $user, Product $product)
    {
        //
    }

    protected function isOwner(User $user, Product $product) {
        return $user->id === $product->user_id;
    }
}
