<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    public function products() {
      return $this->hasMany(Product::class);
    }

    public function isModerator() {
      return $this->isAdministrator() || $this->isOnlyModerator();
    }

    public function isOnlyModerator() {
      return $this->type == 'moderator';
    }

    public function isAdministrator() {
      return $this->type == 'admin';
    }

    static function typesArray() {
      return ['admin' => 'admin', 'moderator' => 'moderator', 'member' => 'member'];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
