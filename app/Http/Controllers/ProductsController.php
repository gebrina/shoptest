<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use DB;
use App\Product;
use App\Brand;
use App\Color;

class ProductsController extends Controller
{
  public function index() {
    // $brand = null;
    if (request('color')) {
      // $color = Color::where('name', request('color'))->firstOrFail();
      $color = Color::where('name', request('color'))->first();
      if ($color) {
        $products = $color->products;
      } else {
        $products = Product::all();
      }
    } else if (request('brand_id')) {
      // $brand = Brand::where('id', request('brand_id'))->firstOrFail();
      $brand = Brand::where('id', request('brand_id'))->first();
      if ($brand) {
        $products = $brand->products;
      } else {
        $products = Product::all();
      }
    } else {
      $products = Product::all();
    }
    // $products = Product::paginate(5);
    return view('products.index', ['products' => $products, 'brand' => $brand ?? null, 'color' => $color ?? null]);
  }

  public function show(Product $product) {
    // $product = Product::where('id', $id)->firstOrFail();
    return view('products.show', compact('product'));
  }

  public function create() {
    return view('products.create', [
      'colors' => Color::all(),
      'brands' => Brand::all()
    ]);
  }

  public function store() {
    $this->validateProduct();
    $product = new Product($this->permittedParams());
    $product->user_id = Auth::user()->id;
    $product->save();
    $product->colors()->attach(request('colors')); // sync() for update?

    return redirect(route('panel.'));
  }

  public function edit(Product $product) {
    $this->authorize('update', $product);
    // $product = Product.find($id);
    return view('products.edit', [
      'product' => $product,
      'colors' => Color::all(),
      'brands' => Brand::all()
    ]);
  }

  public function update(Product $product) {
    $this->authorize('update', $product);
    $product->update($this->validateProduct());
    $product->colors()->sync(request('colors'));
    return redirect(route('panel.'));
  }

  public function destroy(Product $product) {
    $this->authorize('delete', $product);
    $product->delete();
    return redirect(route('panel.'));
  }

  protected function validateProduct() {
    return request()->validate([
      'brand_id' => 'required',
      'name' => 'required',
      'body' => 'required',
      'colors' => 'exists:colors,id'
    ]);
  }

  protected function permittedParams() {
    return request([
      'brand_id',
      'name',
      'body',
      'colors'
    ]);
  }
}
