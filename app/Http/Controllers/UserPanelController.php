<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// use App\User;

class UserPanelController extends Controller
{
  public function show() {
    $products = Auth::user()->products()->get();
    return view('panel.show', ['products' => $products]);
  }
}
