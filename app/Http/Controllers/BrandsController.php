<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;

class BrandsController extends Controller
{

  /**
   * Display the specified resource.
   *
   * @param  \App\Brand  $brand
   * @return \Illuminate\Http\Response
   */
  public function show(Brand $brand)
  {
      $products = $brand->products;
      return view('products.index', compact(['brand', 'products']));
  }

}
