<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
  public function edit(User $user) {
    $this->authorize('update', $user);
    return view('panel.users.edit', ['user' => $user]);
  }

  public function update(User $user) {
    $this->authorize('update', $user);
    $user->update($this->validateUser());
    return redirect(route('panel.'));
  }

  protected function validateUser() {
    return request()->validate([
      'name' => 'required',
      'email' => 'required|email'
    ]);
  }

  protected function permittedParams() {
    return request([
      'name',
      'email'
    ]);
  }
}
