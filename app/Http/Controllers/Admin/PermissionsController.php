<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{
  public function index() {
    // $permissions = Permission::all();
    dd(Permission::all());
    return view('admin.permissions.index', ['permissions' => $permissions]);
  }

  public function create() {
    return view('admin.permissions.create');
  }

  public function store() {
    $this->validatePermission();
    $permission = new Permission($this->permittedParams());
    $permission->save();

    return redirect(route('admin.permissions.index'));
  }

  public function edit(Permission $permission) {
    // $this->authorize('update', $permission);
    return view('admin.permissions.edit', ['permission' => $permission]);
  }

  public function update(Permission $permission) {
    // $this->authorize('update', $permission);
    $permission->update($this->validatePermission());
    return redirect(route('admin.permissions.index'));
  }

  public function destroy(Permission $permission) {
    // $this->authorize('delete', $permission);
    $permission->delete();
    return redirect(route('admin.permissions.index'));
  }

  protected function validatePermission() {
    return request()->validate([
      'name' => 'required',
    ]);
  }

  protected function permittedParams() {
    return request([
      'name',
    ]);
  }
}
