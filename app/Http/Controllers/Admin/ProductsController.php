<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Product;
use App\Brand;
use App\Color;

class ProductsController extends Controller
{
  // public function __construct()
  // {
  //   $this->middleware('moderator'); // moderator or admin
  // }

  public function index() {
    $products = Product::all();
    return view('admin.products.index', ['products' => $products]);
  }

  public function create() {
    return view('products.create', [
      'colors' => Color::all(),
      'brands' => Brand::all()
    ]);
  }

  public function store() {
    $this->validateProduct();
    $product = new Product($this->permittedParams());
    $product->user_id = Auth::user()->id;
    $product->save();
    $product->colors()->attach(request('colors')); // sync() for update?

    return redirect(route('admin.'));
  }

  public function edit(Product $product) {
    return view('admin.products.edit', [
      'product' => $product,
      'colors' => Color::all(),
      'brands' => Brand::all()
    ]);
  }

  public function update(Product $product) {
    // $this->authorize('update', $product);
    $product->update($this->validateProduct());
    $product->colors()->sync(request('colors'));
    return redirect(route('admin.'));
  }

  public function destroy(Product $product) {
    // $this->authorize('delete', $product);
    $product->delete();
    return redirect(route('admin.'));
  }

  protected function validateProduct() {
    return request()->validate([
      'brand_id' => 'required',
      'name' => 'required',
      'body' => 'required',
      'colors' => 'exists:colors,id'
    ]);
  }

  protected function permittedParams() {
    return request([
      'brand_id',
      'name',
      'body',
      'colors'
    ]);
  }
}
