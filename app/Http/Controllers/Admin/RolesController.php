<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesController extends Controller
{
  public function index() {
    $roles = Role::all();
    return view('admin.roles.index', ['roles' => $roles]);
  }

  public function create() {
    return view('admin.roles.create', [
    'permissions' => Permission::all(),
  ]);
  }

  public function store() {
    $this->validateRole();
    $role = new Role($this->permittedParams());
    $permissions = $this->permissionsFromRequest();
    $role->syncPermissions($permissions);
    $role->save();

    return redirect(route('admin.roles.index'));
  }

  public function edit(Role $role) {
    // $this->authorize('update', $role);
    return view('admin.roles.edit', [
      'role' => $role,
      'permissions' => Permission::all(),
    ]);
  }

  public function update(Role $role) {
    // $this->authorize('update', $role);
    $role->update($this->validateRole());
    $permissions = $this->permissionsFromRequest();
    $role->syncPermissions($permissions);
    $role->save();
    return redirect(route('admin.roles.index'));
  }

  public function destroy(Role $role) {
    // $this->authorize('delete', $role);
    $role->delete();
    return redirect(route('admin.roles.index'));
  }

  protected function validateRole() {
    return request()->validate([
      'name' => 'required',
    ]);
  }

  protected function permittedParams() {
    return request([
      'name',
    ]);
  }

  protected function permissionsFromRequest() {
    return Permission::whereIn('id', request()->permissions)->get();
  }
}
