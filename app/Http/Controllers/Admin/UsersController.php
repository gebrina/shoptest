<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\User;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{

  // public function __construct()
  // {
  //   $this->middleware('admin');
  // }

  public function index()
  {
    $response = Gate::inspect('admin', auth()->user());
    if ($response->allowed()) {
      $users = User::all();
      return view('admin.users.index', ['users' => $users]);
    } else {
      return view('admin.no_permission', ['message' => $response->message()]);
    }
    // $this->authorize('admin', auth()->user());
    // $users = User::all();
    // return view('admin.users.index', ['users' => $users]);
  }

  public function edit(User $user) {
    $this->authorize('admin', $user);
    return view('admin.users.edit', [
      'user' => $user,
      'roles' => Role::all()
    ]);
  }

  public function update(User $user) {
    // $this->authorize('update', $user);
    $user->update($this->validateUser());
    $user->syncRoles(Role::find(request()->roles));
    $user->save();
    return redirect(route('admin.users.index'));
  }

  public function destroy(User $user) {
    $this->authorize('admin', $user);
    $user->delete();
    return redirect(route('admin.users.index'));
  }

  protected function validateUser() {
    return request()->validate([
      'name' => 'required',
      'email' => 'required|email',
      'type' => 'required' // TODO: without it it doesn't save
    ]);
  }

  protected function permittedParams() {
    return request([
      'name',
      'email',
      'type'
    ]);
  }

}
