<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Color;

class ColorsController extends Controller
{

  // public function __construct()
  // {
  //   $this->middleware('moderator'); // moderator or admin
  // }

  public function index() {
    $colors = Color::all();
    return view('admin.colors.index', ['colors' => $colors]);
  }

  public function create() {
    return view('admin.colors.create');
  }

  public function store() {
    $this->validateColor();
    $color = new Color($this->permittedParams());
    $color->save();

    return redirect(route('admin.colors.index'));
  }

  public function edit(Color $color) {
    // $this->authorize('update', $color);
    return view('admin.colors.edit', ['color' => $color]);
  }

  public function update(Color $color) {
    // $this->authorize('update', $color);
    $color->update($this->validateColor());
    return redirect(route('admin.colors.index'));
  }

  public function destroy(Color $color) {
    // $this->authorize('delete', $color);
    $color->delete();
    return redirect(route('admin.colors.index'));
  }

  protected function validateColor() {
    return request()->validate([
      'name' => 'required',
      'hex_value' => 'required'
    ]);
  }

  protected function permittedParams() {
    return request([
      'name',
      'hex_value'
    ]);
  }
}
