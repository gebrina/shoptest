<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Brand;

class BrandsController extends Controller
{

  // public function __construct()
  // {
  //   $this->middleware('moderator'); // moderator or admin
  // }

  public function index()
  {
    $this->authorize('index', Brand::class);
    $brands = Brand::all();
    return view('admin.brands.index', ['brands' => $brands]);
  }

  public function create()
  {
    $this->authorize('create', Brand::class);
    return view('admin.brands.create');
  }

  public function store(Request $request)
  {
    $this->authorize('create', Brand::class);
    $this->validateBrand();
    $brand = new Brand($this->permittedParams());
    $brand->save();

    return redirect(route('admin.brands.index'));
  }

  public function edit(Brand $brand)
  {
    $this->authorize('edit', Brand::class);
    return view('admin.brands.edit', ['brand' => $brand]);
  }

  public function update(Request $request, Brand $brand)
  {
    $this->authorize('edit', Brand::class);
    $brand->update($this->validateBrand());
    return redirect(route('admin.brands.index'));
  }

  public function destroy(Brand $brand)
  {
    $this->authorize('edit', Brand::class);
    $brand->delete();
    return redirect(route('admin.brands.index'));
  }

  protected function validateBrand() {
    return request()->validate([
      'name' => 'required',
      'body' => 'required'
    ]);
  }

  protected function permittedParams() {
    return request([
      'name',
      'body'
    ]);
  }
}
