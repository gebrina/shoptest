<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    // relationships
    public function products() {
      return $this->belongsToMany(Product::class);
    }

    public function belongsToProduct($product) {
      return $this->products->contains($product->id);
    }

    protected $fillable = ['name', 'hex_value'];
}
