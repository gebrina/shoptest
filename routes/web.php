<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductsController@index');

// Route::get('/products', 'ProductsController@index')->name('products.index');
// Route::get('/products/create', 'ProductsController@create')->name('products.create');
// Route::post('/products', 'ProductsController@store')->name('products.store');
// Route::get('/products/{product}', 'ProductsController@show')->name('products.show');
// Route::get('/products/{product}/edit', 'ProductsController@edit')->name('products.edit');
// Route::put('/products/{product}', 'ProductsController@update')->name('products.update');
// Route::delete('/products/{product}', 'ProductsController@destroy')->name('products.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/unauthorized', 'HomeController@unauthorized')->name('unauthorized');

Route::resource('products', 'ProductsController')->only(['show', 'index']);
Route::resource('brands', 'BrandsController')->only(['show']);
Route::resource('colors', 'ColorsController')->only(['show']);

Route::name('panel.')->prefix('panel')->middleware('auth', 'member')->group(function () {
  Route::get('/', 'UserPanelController@show');
  Route::resource('products', 'ProductsController')->except(['show', 'index']);
  Route::resource('users', 'UsersController')->only(['edit', 'update']);
});

Route::name('admin.')->prefix('admin')->namespace('Admin')->middleware('auth')->group(function () {
  // Route::get('/', 'DashboardController@index');
  Route::get('/', 'ProductsController@index')->middleware('moderator');
  Route::resource('colors', 'ColorsController')->except(['show'])->middleware('moderator');
  Route::resource('products', 'ProductsController')->except(['show'])->middleware('moderator');
  // admin only
  Route::resource('users', 'UsersController')->except(['create', 'store'])->middleware('admin');
  Route::resource('permissions', 'PermissionsController')->middleware('admin');
  Route::resource('roles', 'RolesController')->middleware('admin');
  // custom permissions
  Route::resource('brands', 'BrandsController')->except(['show']);
});

Imgfly::routes();
