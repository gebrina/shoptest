@extends('layouts.admin')

@section ('content')
  <section class='product'>
    <h1>Dodaj nowy produkt</h1>
    {!! Form::open(['route' => ['admin.products.store'], 'files' => true]) !!}
      @include('products.form', ['product' => null])
    {!! Form::close() !!}
  </section>
@endsection
