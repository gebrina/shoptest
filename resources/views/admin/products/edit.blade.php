@extends('layouts.admin')

@section ('content')
  <section class='product'>
    <h1>Edytuj: {{ $product->name }}</h1>
    {!! Form::model($product, ['route' => ['admin.products.update', $product->id], 'method' => 'PUT', 'files' => true]) !!}
      @include('products.form', ['product' => $product])
    {!! Form::close() !!}
  </section>
@endsection
