@extends('layouts.admin')

@section ('content')
  <section class='product'>
    <h1>Edytuj dane użytkownika</h1>
    {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PUT']) !!}
      <div class="col-12 col-sm-6">
        <div class="form-group form-group-required">
          {!! Form::label('name', 'Nazwa') !!}
          {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
          @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group form-group-required">
          {!! Form::label('email', 'Email') !!}
          {!! Form::text('email', null, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '')]) !!}
          @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="col-12 col-sm-6">
        <div class="form-group form-group-required">
          {!! Form::label('type', 'Typ') !!}
          {!! Form::select('type', App\User::typesArray(), request()->query('type') ?? null, ['class' => 'form-control']) !!}

        </div>
      </div>

      <!-- <div class="control-group @error('type') danger @enderror">
        <label class="control-label" for="type">Typ</label>
        <div class="controls">
          <select class='form-control' name='type' id='type'>
            <option value='admin' {{ ( ( old("type") == 'admin') || (isset($user) && $user->type == 'admin' ) ? "selected":"") }}>Admin</option>
            <option value='moderator' {{ ( ( old("type") == 'moderator') || (isset($user) && $user->type == 'moderator' ) ? "selected":"") }}>Moderator</option>
            <option value='member' {{ ( ( old("type") == 'member') || (isset($user) && $user->type == 'member' ) ? "selected":"") }}>Użytkownik</option>
          </select>
          @error('type')
            <p class="error-field text-danger">{{ $errors->first('type') }}</p>
          @enderror
        </div>
      </div> -->

      <div class="form-group">
        <label class="col-sm-1 control-label" for="roles">Dostępy</label>
        <div class="col-sm-6">
          @foreach ( $roles as $role )
            <div class='checkbox'>
              {!! Form::checkbox('roles[]', $role->id, isset($user) && $user->roles->has($role->id) ? : null, ['id' => 'role_'.$role->id ] ) !!}
              {!! Form::label('role_'.$role->id, $role->name) !!}
            </div>
          @endforeach
        </div>
      </div>

      <div class="control-group">
        <div class="controls">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
      </div>
    {!! Form::close() !!}
  </section>
@endsection
