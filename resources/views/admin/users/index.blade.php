@extends('layouts.admin')

@section ('content')
  <h2>
    Wszystkie margki
  </h2>
  <section class='panel-items'>
    @if ($users->count() > 0)
    <table class="table table-bordered table-striped">
      <thead>
        <th>Id</th>
        <th>Imię</th>
        <th>Email</th>
        <th>Liczba produktów</th>
        <th>Uprawnienia</th>
        <th></th>
      </thead>
      <tbody>
        @foreach ($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->products->count() }}</td>
            <td>
              <span class="label {{ $user->isAdministrator() ? 'label-success' : 'label-danger' }}">Admin</span>
              <span class="label {{ $user->isOnlyModerator() ? 'label-success' : 'label-danger' }}">Mod</span>
              @if ($user->getPermissionsViaRoles()->count() )
                <br>Customowe:<br>
                @foreach ( $user->getPermissionsViaRoles() as $permission )
                  <span class="label label-info }}">{{ $permission->name }}</span>
                @endforeach
              @endif
            </td>
            <td>
              <a href="{{ route('admin.users.edit', $user) }}" class='label label-info'>Edytuj</a>
              <form action="{{ route('admin.users.destroy', $user) }}" method='POST' class="delete-form">
                @csrf
                @method('DELETE')
                <button type="submit" class="label label-danger">Usuń</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    @else
      <p>Brak dodanych kolorów</p>
    @endif
  </section>
@endsection
