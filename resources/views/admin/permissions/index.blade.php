@extends('layouts.admin')

@section ('content')
  <h2>
    Wszystkie kolory
    <a href="{{ route('admin.permissions.create') }}" class='btn btn-primary'>Nowy</a>
  </h2>
  <section class='panel-items'>
    @if ($permissions->count() > 0)
    <table class="table table-bordered table-striped">
      <thead>
        <th>Nazwa</th>
        <th>Rola</th>
        <th></th>
      </thead>
      <tbody>
        @foreach ($permissions as $permission)
          <tr>
            <td>
              {{ $permission->name }}
            </td>
            <td>{{ $permission->roles()->count() }}</td>
            <td>
              <a href="{{ route('admin.permissions.edit', $permission) }}" class='label label-info'>Edytuj</a>
              <form action="{{ route('admin.permissions.destroy', $permission) }}" method='POST' class="delete-form">
                @csrf
                @method('DELETE')
                <button type="submit" class="label label-danger">Usuń</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    @else
      <p>Brak dodanych kolorów</p>
    @endif
  </section>
@endsection
