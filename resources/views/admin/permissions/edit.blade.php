@extends('layouts.admin')

@section ('content')
  <section>
    <h1>Edytuj: {{ $permission->name }}</h1>
    <form action="{{ route('admin.permissions.update', $permission) }}" method='POST' class="form-horizontal">
      @csrf
      @method('PUT')

      @include('admin.permissions._form')
      <div class="form-group">
        <div class="col-sm-offset-1 col-sm-6 ">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
      </div>
    </form>
  </section>
@endsection
