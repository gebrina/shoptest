<div class="form-group @error('name') danger @enderror">
  <label class="col-sm-1 control-label" for="name">Nazwa</label>
  <div class="col-sm-6">
    <input class='form-control' type="text" name="name" id="name" placeholder="Nazwa" value="{{ old('name') ? old('name') : $color->name }}">
    @error('name')
      <p class="error-field text-danger">{{ $errors->first('name') }}</p>
    @enderror
  </div>
</div>
