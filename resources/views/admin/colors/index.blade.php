@extends('layouts.admin')

@section ('content')
  <h2>
    Wszystkie kolory
    <a href="{{ route('admin.colors.create') }}" class='btn btn-primary'>Nowy</a>
  </h2>
  <section class='panel-items'>
    @if ($colors->count() > 0)
    <table class="table table-bordered table-striped">
      <thead>
        <th>Kolor</th>
        <th>Nazwa</th>
        <th>Liczba produktów</th>
        <th></th>
      </thead>
      <tbody>
        @foreach ($colors as $color)
          <tr>
            <td>
              <span class='color-box' title="{{ $color->name}}" style="background-color: {{ $color->hex_value}}"></span>
            </td>
            <td>
              {{ $color->name }}
              <br>
              {{ $color->hex_value }}
            </td>
            <td>{{ $color->products()->count() }}</td>
            <td>
              <a href="{{ route('admin.colors.edit', $color) }}" class='label label-info'>Edytuj</a>
              <form action="{{ route('admin.colors.destroy', $color) }}" method='POST' class="delete-form">
                @csrf
                @method('DELETE')
                <button type="submit" class="label label-danger">Usuń</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    @else
      <p>Brak dodanych kolorów</p>
    @endif
  </section>
@endsection
