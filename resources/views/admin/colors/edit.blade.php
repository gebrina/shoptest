@extends('layouts.admin')

@section ('content')
  <section>
    <h1>Edytuj: {{ $color->name }}</h1>
    <form action="{{ route('admin.colors.update', $color) }}" method='POST' class="form-horizontal">
      @csrf
      @method('PUT')
      <div class="form-group @error('name') danger @enderror">
        <label class="col-sm-1 control-label" for="name">Nazwa</label>
        <div class="col-sm-6">
          <input class='form-control' type="text" name="name" id="name" placeholder="Nazwa" value="{{ old('name') ? old('name') : $color->name }}">
          @error('name')
            <p class="error-field text-danger">{{ $errors->first('name') }}</p>
          @enderror
        </div>
      </div>
      <div class="form-group @error('hex_value') danger @enderror">
        <label class="col-sm-1 control-label" for="hex_value">Wartość HEX</label>
        <div class="col-sm-6">
          <input class='form-control' type="text" name="hex_value" id="hex_value" placeholder="#ffffff" value="{{ old('hex_value') ? old('hex_value') : $color->hex_value }}">
          @error('hex_value')
            <p class="error-field text-danger">{{ $errors->first('hex_value') }}</p>
          @enderror
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-1 col-sm-6 ">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
      </div>
    </form>
  </section>
@endsection
