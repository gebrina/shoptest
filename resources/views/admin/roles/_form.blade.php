<div class="col-12 col-sm-6">
  <div class="form-group form-group-required">
    {!! Form::label('name', 'Nazwa') !!}
    {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
    @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('name') }}</strong>
      </span>
    @endif
  </div>
</div>

<div class="form-group">
  <label class="col-sm-1 control-label" for="permissions">Dostępy</label>
  <div class="col-sm-6">
    @foreach ( $permissions as $permission )
      <div class='checkbox'>
        {!! Form::checkbox('permissions[]', $permission->id, isset($role) && $role->permissions->has($permission->id) ? : null, ['id' => 'permission_'.$permission->id ] ) !!}
        {!! Form::label('permission_'.$permission->id, $permission->name) !!}
      </div>
    @endforeach
  </div>
</div>

<div class="col-12 col-sm-6">
  {!! Form::submit('Zapisz') !!}
</div>
