@extends('layouts.admin')

@section ('content')
  <section>
    <h1>Edytuj: {{ $role->name }}</h1>
    {!! Form::model($role, ['route' => ['admin.roles.update', $role->id], 'method' => 'PUT']) !!}

      @include('admin.roles._form')
      
    {!! Form::close() !!}
  </section>
@endsection
