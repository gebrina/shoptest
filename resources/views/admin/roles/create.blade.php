@extends('layouts.admin')

@section ('content')
  <section>
    <h1>Dodaj nową rolę</h1>
    {!! Form::open(['route' => ['admin.roles.store']]) !!}
      @include('admin.roles._form')
    </form>
  </section>
@endsection
