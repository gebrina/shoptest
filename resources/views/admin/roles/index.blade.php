@extends('layouts.admin')

@section ('content')
  <h2>
    Wszystkie kolory
    <a href="{{ route('admin.roles.create') }}" class='btn btn-primary'>Nowy</a>
  </h2>
  <section class='panel-items'>
    @if ($roles->count() > 0)
    <table class="table table-bordered table-striped">
      <thead>
        <th>Nazwa</th>
        <th>Liczba dostępów</th>
        <th></th>
      </thead>
      <tbody>
        @foreach ($roles as $role)
          <tr>
            <td>
              {{ $role->name }}
            </td>
            <td>{{ $role->permissions()->count() }}</td>
            <td>
              <a href="{{ route('admin.roles.edit', $role) }}" class='label label-info'>Edytuj</a>
              <form action="{{ route('admin.roles.destroy', $role) }}" method='POST' class="delete-form">
                @csrf
                @method('DELETE')
                <button type="submit" class="label label-danger">Usuń</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    @else
      <p>Brak dodanych dostępów</p>
    @endif
  </section>
@endsection
