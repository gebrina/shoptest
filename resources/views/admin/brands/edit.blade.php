@extends('layouts.admin')

@section ('content')
  <section>
    <h1>Edytuj: {{ $brand->name }}</h1>
    <form action="{{ route('admin.brands.update', $brand) }}" method='POST' class="form-horizontal">
      @csrf
      @method('PUT')
      <div class="form-group @error('name') danger @enderror">
        <label class="col-sm-1 control-label" for="name">Nazwa</label>
        <div class="col-sm-6">
          <input class='form-control' type="text" name="name" id="name" placeholder="Nazwa" value="{{ old('name') ? old('name') : $brand->name }}">
          @error('name')
            <p class="error-field text-danger">{{ $errors->first('name') }}</p>
          @enderror
        </div>
      </div>
      <div class="form-group @error('body') danger @enderror">
        <label class="col-sm-1 control-label" for="body">Opis</label>
        <div class="col-sm-6">
          <textarea class='form-control' rows='3' name='body' id="body">{{ old('body') ? old('body') : $brand->body }}</textarea>
          @error('body')
            <p class="error-field text-danger">{{ $errors->first('body') }}</p>
          @enderror
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-1 col-sm-6 ">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
      </div>
    </form>
  </section>
@endsection
