@extends('layouts.admin')

@section ('content')
  <section>
    <h1>Dodaj nowy kolor</h1>
    <form action="{{ route('admin.brands.store') }}" method='POST' class="form-horizontal">
      @csrf
      <div class="form-group @error('name') danger @enderror">
        <label class="col-sm-1 control-label" for="name">Nazwa</label>
        <div class="col-sm-6">
          <input class='form-control' type="text" name="name" id="name" placeholder="Nazwa" value="{{ old('name') }}">
          @error('name')
            <p class="error-field text-danger">{{ $errors->first('name') }}</p>
          @enderror
        </div>
      </div>
      <div class="form-group @error('body') danger @enderror">
        <label class="col-sm-1 control-label" for="body">Opis</label>
        <div class="col-sm-6">
          <textarea class='form-control' rows='3' name='body' id="body">{{ old('body') }}</textarea>
          @error('body')
            <p class="error-field text-danger">{{ $errors->first('body') }}</p>
          @enderror
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-1 col-sm-6 ">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
      </div>
    </form>
  </section>
@endsection
