@extends('layouts.admin')

@section ('content')
  <h2>
    Wszystkie margki
    <a href="{{ route('admin.brands.create') }}" class='btn btn-primary'>Nowy</a>
  </h2>
  <section class='panel-items'>
    @if ($brands->count() > 0)
    <table class="table table-bordered table-striped">
      <thead>
        <th>Logo</th>
        <th>Nazwa</th>
        <th>Opis</th>
        <th></th>
      </thead>
      <tbody>
        @foreach ($brands as $brand)
          <tr>
            <td>
              <a href="{{ route('admin.brands.edit', $brand) }}" class='image-box'>
                <img src=''>
              </a>
            </td>
            <td>
              {{ $brand->name }}
            </td>
            <td>{{ Str::limit($brand->body, $limit = 150, $end = '...') }}</td>
            <td>
              <a href="{{ route('admin.brands.edit', $brand) }}" class='label label-info'>Edytuj</a>
              <form action="{{ route('admin.brands.destroy', $brand) }}" method='POST' class="delete-form">
                @csrf
                @method('DELETE')
                <button type="submit" class="label label-danger">Usuń</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    @else
      <p>Brak dodanych kolorów</p>
    @endif
  </section>
@endsection
