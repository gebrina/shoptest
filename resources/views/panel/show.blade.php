@extends('layouts.app')

@section ('content')
  <h2>
    Moje produkty
    <a href="{{ route('panel.products.create') }}" class='btn btn-primary'>Nowy</a>
    <a href="{{ route('panel.users.edit', auth()->user()) }}" class='btn btn-success'>Moje dane</a>
  </h2>
  <section class='panel-items'>
    @if ($products->count() > 0)
    <table class="table table-bordered table-striped">
      <thead>
        <th>Zdjecie</th>
        <th>Nazwa</th>
        <th>Opis</th>
        <th>Kolory</th>
        <th></th>
      </thead>
      <tbody>
        @foreach ($products as $product)
          <tr>
            <td>
              <a href="{{ route('products.show', $product) }}" class='image-box'>
                <img src=''>
              </a>
            </td>
            <td>
              <a href="{{ route('products.show', $product) }}" class='table-link'>{{ $product->name }}</a>
            </td>
            <td>{{ Str::limit($product->body, $limit = 150, $end = '...') }}</td>
            <td>
              @foreach ( $product->colors as $color )
                <span class="label label-info" style="background-color: {{ $color->hex_value }}">{{ $color->name }}</span>
              @endforeach
            </td>
            <td>
              <a href="{{ route('panel.products.edit', $product) }}" class='label label-info'>Edytuj</a>
              <form action="{{ route('panel.products.destroy', $product) }}" method='POST' class="delete-form">
                @csrf
                @method('DELETE')
                <button type="submit" class="label label-danger">Usuń</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    @else
      <p>Brak dodanych produktów</p>
    @endif
  </section>
@endsection
