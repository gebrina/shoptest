@extends('layouts.app')

@section ('content')
  <section class='product'>
    <h1>Edytuj swoje dane</h1>
    <form action="{{ route('panel.users.update', $user) }}" method='POST' class="form-horizontal">
      @csrf
      @method('PUT')
      <div class="control-group @error('name') danger @enderror">
        <label class="control-label" for="name">Nazwa</label>
        <div class="controls">
          <input type="text" name="name" id="name" placeholder="Nazwa" value="{{ old('name') ? old('name') : $user->name }}">
          @error('name')
            <p class="error-field text-danger">{{ $errors->first('name') }}</p>
          @enderror
        </div>
      </div>
      <div class="control-group @error('email') danger @enderror">
        <label class="control-label" for="email">Email</label>
        <div class="controls">
          <input type="text" name="email" id="email" placeholder="Nazwa" value="{{ old('email') ? old('email') : $user->email }}">
          @error('email')
            <p class="error-field text-danger">{{ $errors->first('email') }}</p>
          @enderror
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
      </div>
    </form>
  </section>
@endsection
