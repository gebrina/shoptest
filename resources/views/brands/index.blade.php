@extends('layouts.app')

@section ('content')
  <section class='admin-items'>
    <table class="table table-bordered table-striped">
      <thead>
        <th>Id</th>
        <th>Nazwa</th>
        <th>Opis</th>
        <th>L. produktów</th>
        <th></th>
      </thead>
      <tbody>
        @forelse ($items as $item)
          <tr>
            <td>{{ $item->id }}</td>
            <td>
              <a href="{{ route('products.index', ['brand_id'=>$item->id]) }}" class='table-link'>{{ $item->name }}</a>
            </td>
            <td>{{ Str::limit($item->body, $limit = 150, $end = '...') }}</td>
            <td>{{ $item->products->count() }}</td>
          </tr>
        @empty
          <tr>Brak wprowadzonych danych</tr>
        @endforelse
      </tbody>
    </table>
  </section>
@endsection
