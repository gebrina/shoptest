@extends('layouts.app')

@section('content')
  <div class="flex-center position-ref full-height">
    <div class="code">Unauthorized</div>
    <div class="message" style="padding: 10px;">Access only with proper permission</div>
  </div>
@endsection
