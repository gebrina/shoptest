@extends('layouts.app')

@section ('content')
  <h2>
    @if ( $brand )
      Produkty marki <strong>{{ $brand->name }}</strong>
    @elseif ( $color )
      Produkty w kolorze <strong style="color: {{ $color->hex_value }}">{{ $color->name }}</strong>
    @else
      Wszystkie produkty
    @endif
  </h2>
  <section class='products'>
    @forelse ($products as $product)
      <div class='product'>
        <a href="{{ route('products.show', $product) }}" class='image-box'>
          <img src=''>
        </a>
        <div class='product-info-box'>
          <a href="{{ route('products.show', $product) }}" class='product'>
            <h3>{{ $product->name }}</h3>
          </a>
          <p>{{ Str::limit($product->body, $limit = 150, $end = '...') }}</p>
        </div>
      </div>
    @empty
      <p>No products</p>
    @endforelse
  </section>
@endsection
