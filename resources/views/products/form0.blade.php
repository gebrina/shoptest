<form action="{{ route('panel.products.update', $product) }}" method='POST' class="form-horizontal">
  @csrf
  @method('PUT')
  <div class="form-group @error('brand_id') danger @enderror">
    <label class="col-sm-1 control-label" for="brand_id">Marka</label>
    <div class="col-sm-6">
      <select class="form-control" name='brand_id'>
        @foreach ( $brands as $brand )
          <option value="{{ $brand->id }}" @if (( $product->brand_id == $brand->id ) || (old('brand_id') == $brand->id )) selected="selected" @endif >{{ $brand->id }} {{ $brand->name }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group @error('name') danger @enderror">
    <label class="col-sm-1 control-label" for="name">Nazwa</label>
    <div class="col-sm-6">
      <input class='form-control' type="text" name="name" id="name" placeholder="Nazwa" value="{{ old('name') ? old('name') : $product->name }}">
      @error('name')
        <p class="error-field text-danger">{{ $errors->first('name') }}</p>
      @enderror
    </div>
  </div>
  <div class="form-group @error('body') danger @enderror">
    <label class="col-sm-1 control-label" for="body">Opis</label>
    <div class="col-sm-6">
      <textarea class='form-control' rows='3' name='body' id="body">{{ old('body') ? old('body') : $product->body }}</textarea>
      @error('body')
        <p class="error-field text-danger">{{ $errors->first('body') }}</p>
      @enderror
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-1 control-label" for="colors">Kolory</label>
    <div class="col-sm-6">
      @foreach ( $colors as $color )
        <div class='checkbox'>
          <label for="color_ids_{{ $color->id }}">
            <input class="check_boxes optional" type="checkbox" value="{{ $color->id }}" name="colors[]" id="color_ids_{{ $color->id }}"
              @if ( (is_array(old('colors')) && in_array($color->id, old('colors'))) || ( $color->belongsToProduct($product) ) )
                checked
              @endif
            >
            {{ $color->name }}
          </label>
        </div>
      @endforeach
      @error('colors')
        <p class="error-field text-danger">{{ $errors->first('colors') }}</p>
      @enderror
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-6 ">
      <button type="submit" class="btn btn-primary">Zapisz</button>
    </div>
  </div>
</form>
