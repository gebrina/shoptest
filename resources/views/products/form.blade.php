<div class="col-12 col-sm-6">
  <div class="form-group form-group-required">
    {!! Form::label('brand_id', 'Marka') !!}
    {!! Form::select('brand_id', $brands->pluck('name', 'id')->toArray(), request()->query('brand_id') ?? null, ['placeholder' => 'Wybierz markę...', 'class' => 'form-control']) !!}

  </div>
</div>

<div class="col-12 col-sm-6">
  <div class="form-group form-group-required">
    {!! Form::label('image', 'Zjęcie') !!}
    {!! Form::file('image'); !!}
    @if ($errors->has('image'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('image') }}</strong>
        </span>
    @endif

    @isset ($product->image)
    <p>
        <img src="{{ image_thumb($product->image, 'w=300') }}" alt="" class="img-thumbnail mt-1" />
    </p>
    @endif
  </div>
</div>

<div class="col-12 col-sm-6">
  <div class="form-group form-group-required">
    {!! Form::label('name', 'Nazwa') !!}
    {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
    @if ($errors->has('name'))
      <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('name') }}</strong>
      </span>
    @endif
  </div>
</div>
<div class="col-12 col-sm-6">
  <div class="form-group form-group-required">
    {!! Form::label('body', 'Opis') !!}
    {!! Form::textarea('body', null, ['rows' => 6, 'class' => 'wysiwyg form-control'.($errors->has('body') ? ' is-invalid' : '')]) !!}

    @if ($errors->has('body'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('body') }}</strong>
      </span>
    @endif
  </div>
</div>
<div class="form-group">
  <label class="col-sm-1 control-label" for="colors">Kolory</label>
  <div class="col-sm-6">
    @foreach ( $colors as $color )
      <div class='checkbox'>
        {!! Form::checkbox('colors[]', $color->id, isset($product) && $product->colors->has($color->id) ? : null, ['id' => 'color_'.$color->id ] ) !!}
        {!! Form::label('color_'.$color->id, $color->name) !!}
      </div>
    @endforeach
  </div>
</div>

<div class="col-12 col-sm-6">
  {!! Form::submit('Zapisz') !!}
</div>
