@extends('layouts.app')

@section ('content')
  <section class='product'>
    <h1>{{ $product->name }}</h1>
    <p>{{ $product->body }}</p>
    @foreach ( $product->colors as $color )
      <a href="{{ route('products.index', ['color'=>$color->name]) }}">
        <span class="label label-info" style="background-color: {{ $color->hex_value }}">{{ $color->name }}</span>
      </a>
    @endforeach
    <br>
    <h4>Marka</h4>
    <a href="{{ route('products.index', ['brand_id'=>$product->brand->id]) }}">
      {{ $product->brand->name }}
    </a>
  </section>
@endsection
