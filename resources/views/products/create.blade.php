@extends('layouts.app')

@section ('content')
  <section class='product'>
    <h1>Dodaj nowy produkt</h1>
    @if ( auth()->user()->isModerator() )
      {!! Form::open(['route' => ['admin.products.store'], 'files' => true]) !!}
    @else
      {!! Form::open(['route' => ['panel.products.store'], 'files' => true]) !!}
    @endif

      @include('products.form', ['product' => null])
    {!! Form::close() !!}
  </section>
@endsection
