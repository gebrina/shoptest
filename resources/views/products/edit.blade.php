@extends('layouts.app')

@section ('content')
  <section class='product'>
    <h1>Edytuj: {{ $product->name }}</h1>
    @if ( auth()->user()->isModerator() )
      {!! Form::model($product, ['route' => ['admin.products.update', $product->id], 'method' => 'PUT', 'files' => true]) !!}
    @else
      {!! Form::model($product, ['route' => ['panel.products.update', $product->id], 'method' => 'PUT', 'files' => true]) !!}
    @endif

      @include('products.form', ['product' => $product])
    {!! Form::close() !!}
  </section>
@endsection
