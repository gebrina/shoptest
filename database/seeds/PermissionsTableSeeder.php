<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Arr;


class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $items = [ 'brand.index', 'brand.create', 'brand.edit'];

        Permission::create([ 'name' => 'brand.index', 'guard_name' => 'web' ]);
        Permission::create([ 'name' => 'brand.create', 'guard_name' => 'web' ]);
        Permission::create([ 'name' => 'brand.edit', 'guard_name' => 'web' ]);
        // foreach ($items as $item)
        // {
        //     Permission::create([
        //       'name' => $item,
        //       'guard_name' => 'web',
        //     ]);
        // }

        $admin = Role::create([
          'name' => 'admin',
          'guard_name' => 'web',
        ]);

        $moderator = Role::create([
          'name' => 'moderator',
          'guard_name' => 'web',
        ]);

        $admin->syncPermissions(Permission::all());
        $moderator->syncPermissions(Permission::first());
    }
}
