<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Administrator',
                'email' => 'admin@shoptest.pl',
                'password' => bcrypt('Test123!'),
                'type' => 'admin'
            ],
            [
                'name' => 'Moderator',
                'email' => 'moderator@shoptest.pl',
                'password' => bcrypt('Test123!'),
                'type' => 'moderator'
            ],
            [
                'name' => 'Normal User',
                'email' => 'normal_user@shoptest.pl',
                'password' => bcrypt('Test123!'),
                'type' => 'member'
            ]
        ];

        foreach ($items as $item)
        {
            $user = App\User::create($item);
            if ($user->isAdministrator()) {
                $user->assignRole(['admin']);
            } else if ($user->isOnlyModerator()) {
              $user->assignRole(['moderator']);
            }
        }
    }
}
